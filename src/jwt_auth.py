from typing import Dict
import jwt
import datetime


class JWTConfig(object):
    key = 'some_key'
    algorithm = 'HS256'


class Token(JWTConfig):
    def __init__(self, payload: Dict):
        self.payload = self._set_exp_date(payload)

    def _set_exp_date(self, payload: Dict):
        payload.update(exp=datetime.datetime.utcnow() + datetime.timedelta(seconds=40))
        return payload

    def _check_exp(self, expire_date):
        # expire_date is UnixTimeStamp
        when_exp = datetime.datetime.fromtimestamp(expire_date)
        if datetime.datetime.now() >= when_exp:
            raise jwt.ExpiredSignatureError()

    def create(self):
        return jwt.encode(payload=self.payload, key=self.key, algorithm=self.algorithm)

    def get_payload(self, token):
        payload = jwt.decode(token, self.key, self.algorithm)
        self._check_exp(payload.get('exp'))
        return payload

t = Token({'lol': 'kek'})
acc = t.create()
t.get_payload(acc)