from marshmallow import Schema, fields
from marshmallow.validate import Length


class LoginSchema(Schema):
    username = fields.Str(validate=Length(min=4, max=20))
    password = fields.Str(validate=Length(min=4, max=255), required=True)