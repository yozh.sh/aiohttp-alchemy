import functools


def async_query(function):
    """
    Simple wrapper around run async queries in SQLAlchemy
    return future from session.run_sync
    please dont forget await this
    """
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        from .models import session
        return session.run_sync(function, *args, **kwargs)
    return wrapper
