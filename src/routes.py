from aiohttp import web
from src.views.login import login_view
from src.views.registration import registration_view


urls = [
    web.get('/login', login_view),
    web.post('/registration', registration_view)
]