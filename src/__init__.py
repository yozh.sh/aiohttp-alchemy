from aiohttp import web
from .routes import urls


async def factory_app():
    app = web.Application()
    app.add_routes(urls)
    return app
