from src.schemas.login import LoginSchema
from marshmallow import ValidationError
from aiohttp import web


async def login_view(request):
    schema = LoginSchema()
    try:
        data = schema.load(await request.json())
    except ValidationError as error:
        return web.json_response(error.messages)

    return web.json_response(schema.dump(data))