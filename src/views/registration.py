from aiohttp import web
from src.schemas.registration import RegistrationSchema
from marshmallow.validate import ValidationError
from src.models.user import User


async def registration_view(request):
    schema = RegistrationSchema()
    try:
        validated = schema.load(await request.json())
    except ValidationError as error:
        return web.json_response(error.messages)

    # check user
    user = await User.get_user(username=validated.get('username'))
    if user:
        return web.json_response(data={'error': 'user already exist'})

    await User.create_user(username=validated.get('username'), password=validated.get('password'))

    new_user = await User.get_user(username=validated.get('username'))
    return web.json_response(data=schema.dump(new_user))