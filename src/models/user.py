from .base import BaseModel
from sqlalchemy import Column, String
import bcrypt
from src.utils import async_query


class User(BaseModel):
    __tablename__ = 'users'

    username = Column(String(length=20), unique=True, index=True)
    _password = Column(String(length=60))

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, plain_text_password: str):
        self._password = bcrypt.hashpw(plain_text_password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')

    def check_password(self, password: str):
        return bcrypt.checkpw(password.encode('utf-8'), self._password)

    @staticmethod
    @async_query
    def get_user(session=None, username=None):
        return session.query(User).filter(User.username==username).first()

    @staticmethod
    @async_query
    def create_user(session, username: str, password: str):
        user = User(username=username, password=password)
        session.add(user)
        session.commit()
        return user

    def __repr__(self):
        return self.username


