from sqlalchemy.ext.asyncio import create_async_engine
from src.settings import DB_DSN
from sqlalchemy.ext.asyncio import AsyncSession
from .meta import Base


# MODEL IMPORTS
from .user import User

engine = create_async_engine(DB_DSN)

session = AsyncSession(bind=engine)


async def init_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)